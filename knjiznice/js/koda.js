
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var bmiUrl = 'https://bmi.p.mashape.com/';

var bmiToken = 'f1fFCd1LLOmshrtxS1P6tnhUsUIup1Hua5ijsn4iJkSFUs31BK';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
    sessionId = getSessionId();
  ehrId = "";

  var ime = "";
  var priimek = "";
  var spol = "";
  var datumRojstva = "";
  var visina = 0;
  var teza = 0;
  var d = new Date();
  var datumDanes = d.toISOString();

  // TODO: Potrebno implementirati
  switch (stPacienta) {
    case 1:
        ime = "Gregor";
        priimek = "Štefanič";
        spol = "MALE";
        datumRojstva = "1997-10-16T21:00";
        visina = 195;
        teza = 95;
        break;
    case 2:
        ime = "Janez";
        priimek = "Svetokriški";
        spol = "MALE";
        datumRojstva = "1947-10-14T13:13";
        visina = 173;
        teza = 66;
        break;
    case 3:
        ime = "Alice";
        priimek = "Wonderland";
        spol = "FEMALE";
        datumRojstva = "1956-11-12T10:19";
        visina = 156;
        teza = 46;
      default:
          break;
  }

  // TODO: AJAX klic
  $.ajaxSetup({
      headers: {"Ehr-Session": sessionId}
  });
  var response = $.ajax({
      url: baseUrl + "/ehr",
      async: false,
      type: 'POST',
      success: function (data) {
          ehrId = data.ehrId;
          var partyData = {
              firstNames: ime,
              lastNames: priimek,
              gender: spol,
              dateOfBirth: datumRojstva,
              partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
          };
          $.ajax({
              url: baseUrl + "/demographics/party",
              type: 'POST',
              contentType: 'application/json',
              data: JSON.stringify(partyData),
              success: function (party) {
                  // TODO: prikazi obvestilo na zaslon
                  $("#preberiPredlogoBolnika").append(" \
                    <option value='" + ime + "," + priimek + "," + spol + "," + datumRojstva + "'>" + ime + " " + priimek + "</option> \
                  ");

              },
              error: function(err) {
                  $("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
              }
          });
      }
  });

  ehrId = response.responseJSON.ehrId;
  $("#preberiObstojeciEHR").append(" \
    <option value='" + ehrId + "'>" + ime + " " + priimek + "</option> \
  ");

  $("#preberiEhrIdZaVitalneZnake").append(" \
    <option value='" + ehrId + "'>" + ime + " " + priimek + "</option> \
  ");

  $("#obvestiloNovUporabnik").append("<div id='obvestilo" + stPacienta + "' class=\"alert alert-success alert-dismissible\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>Dodan nov uporabnik " + ime + " " + priimek + " (<strong>" + ehrId +"</strong>) </div>");
  $("#obvestilo" + stPacienta).fadeTo(5000, 500).slideUp(500, function(){
    $("#obvestilo" + stPacienta).slideUp(500);
  });


    $("#preberiObstojeciVitalniZnak").append(" \
    <option value='" + ehrId + "," + datumDanes + "," + visina + "," + teza + "'>" + ime + " " + priimek + "</option>");

    // vnos nakljucnih meritev
    for (var i = 0; i < 10; i++) {
        visina = Math.floor(Math.random() * 2 + visina - 1);
        teza = Math.floor(Math.random() * 15 + teza - 6);
        datumDanes = randomTimeDate();
        var starost = new Date(d - new Date(datumRojstva).getFullYear()-1970);

        var bmi = teza / (visina * visina) * 10000;
        bmi = Math.round(bmi * 10) / 10;
        console.log(bmi);
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumDanes,
		    "vital_signs/height_length/any_event/body_height_length": visina,
            "vital_signs/body_mass_index/any_event/body_mass_index": parseFloat(bmi),
		    "vital_signs/body_weight/any_event/body_weight": teza
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
            async: false,
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {

		    },
		    error: function(err) {

		    }
		});
    }

  return ehrId;
}

// nakljucni cas - ZACETEK

function randomTimeDate()
{
	// 2014-12-11T22:41:51.328+01:00
	var leto = Math.floor(Math.random()*3+2014);
	var mesec = Math.floor(Math.random()*12+1);
	var danZgM;
	if(mesec == 1 || mesec == 3 || mesec == 5 || mesec == 7 || mesec == 8 || mesec == 10 || mesec == 12)
		danZgM = 31;
	else if(mesec == 2)
		danZgM = 28;
	else
		danZgM = 30;

	var dan = Math.floor(Math.random()*danZgM+1);
	var ura = Math.floor(Math.random()*23);
	var minuta = Math.floor(Math.random()*60+1);
	var sekunda = Math.floor(Math.random()*60);
	// var tisocinka = Math.floor(Math.random()*1000);

	var datum = leto + "-" + mesec + "-" + dan + "T" + ura + ":" + minuta + ":" + sekunda +	 "Z";
    console.log(datum);
	return datum;

}

// nakljucni cas - KONEC

function generiranjePodatkov() {
    $("#preberiPredlogoBolnika").html("<option value=''></option>");
    $("#preberiObstojeciEHR").html("<option value=''></option>");
    $("#preberiObstojeciVitalniZnak").html("<option value=''></option>");
    $("#preberiEhrIdZaVitalneZnake").html("<option value=''></option>");
    $("#obvestiloNovUporabnik").html("");

	console.log(generirajPodatke(1));
    console.log(generirajPodatke(2));
    console.log(generirajPodatke(3));

}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

// Kreiraj EHR za uporabnika ZACETEK

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var spol = $("#kreirajSpol").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 ||
	  datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
					gender: spol,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

// Kreiraj EHR za uporabnika KONEC

// Preberi EHR za uporabnika - ZACETEK

/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
                var birthDate = party.dateOfBirth.split("T");
                var gender = party.gender.startsWith("FEMALE") ? "Ž" : "M";
				$("#preberiSporocilo").html("<span class='obvestilo label " +
                  "label-success fade-in'>Bolnik '" + party.firstNames + " " +
                  party.lastNames + "' (" + gender + ")" + ", ki se je rodil '" + birthDate[0] +
                  "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

// Preberi EHR za uporabnika - KONEC

function izracunajBMI(spol, starost, teza, visina) {
  sessionId = getSessionId();
  spol = spol.charAt(0);

    var podatki = {
        "weight":{
            "value": teza,
            "unit":"kg"},
        "height":{
            "value": visina,
            "unit":"cm"
        },
        "sex": spol,
        "age": starost
      };
    var response = $.ajax({
        method: "POST",
        url: "https://bmi.p.mashape.com/",
        async: false,
        headers: {
            "X-Mashape-Key": "f1fFCd1LLOmshrtxS1P6tnhUsUIup1Hua5ijsn4iJkSFUs31BK",
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        data: JSON.stringify(podatki),
        success: function (data) {
        }
    });

    //console.log(response);

    return response.responseJSON.bmi.value;
}

// dodajMeritveVitalnihZnakov - ZACETEK

/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritve() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();

    var response = $.ajax({
        url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
        type: 'GET',
        async: false,
        headers: {"Ehr-Session": sessionId},
        success: function (data) {

        }
    });
    var spol = response.responseJSON.party.gender.toLowerCase();
    var starost = new Date(new Date - new Date(response.responseJSON.party.dateOfBirth)).getFullYear()-1970


    var bmi = izracunajBMI(spol, starost, telesnaTeza, telesnaVisina);
    console.log(bmi);

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
            "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
            "vital_signs/body_mass_index/any_event/body_mass_index": parseFloat(bmi),
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              "Meritve uspešno zabeležene" + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function izrisiMeritveBmi() {
	sessionId = getSessionId();

    var arrBMI = [];

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
        //
        $.ajaxSetup({
            headers: {
                "Ehr-Session": sessionId
            }
        });

        var aql =
        "select \
        t/data[at0001]/events[at0006]/time/value as cas, \
        t/data[at0001]/events[at0002]/data[at0003]/items[at0004]/value as bmi \
        from EHR e[e/ehr_id/value='" + ehrId + "'] \
        contains OBSERVATION t[openEHR-EHR-OBSERVATION.body_mass_index.v1] \
        order by t/data[at0001]/events[at0006]/time/value desc \
        limit 31";

        var response = $.ajax({
            url: baseUrl + "/query?" + $.param({"aql": aql}),
            type: 'GET',
            async: false,
            success: function (res) {
                //console.log(res.resultSet);
            }
        });
        //console.log(response.responseJSON);

        var arrBMI = [];
        for (var i = 0; i < response.responseJSON.resultSet.length; i++) {
            arrBMI.push(response.responseJSON.resultSet[i].bmi.magnitude);
        }
        console.log(arrBMI);

        narisiGraf(arrBMI);
	}

}
// preberiMeritveVitalnihZnakov - KONEC


// Graf - ZACETEK
function narisiGraf(podatki, ehrId) {
    $("#ogrodjeGrafa").html('<div class="panel panel-default">\
        <div class="panel-heading">\
            <div class="row">\
                <div class="col-lg-8 col-md-8 col-sm-8">Pregled meritev <b>indeksa telesne mase (ITM)</b> uporabnika</div>\
            </div>\
        </div>\
        <div class="panel-body">\
            <div id="graf" class="col-lg-12 col-md-12 col-sm-12">\
            </div>\
        </div>\
    </div>');
    var margin = {top: 40, right: 20, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

    var formatPercent = d3.format("");

    var x = d3.scale.ordinal()
        .rangeRoundBands([0, width], .1);

    var y = d3.scale.linear()
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickFormat(formatPercent);

    var tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function(d) {
        return "<strong>BMI:</strong> <span style='color:red'>" + d.frequency + "</span>";
      })

    var svg = d3.select("#graf").append("svg")
        .attr("width", "100%")
        .attr("height", height + margin.top + margin.bottom)
        //.attr("class", "col-lg-12 col-md-12 col-sm-12")
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.call(tip);
    data = [];
    for (var i = 0; i < podatki.length; i++) {
        data.push({letter: i, frequency: podatki[i]});
    }


    x.domain(data.map(function(d) { return d.letter; }));
      y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

      svg.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis);

      svg.append("g")
          .attr("class", "y axis")
          .call(yAxis)
        .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", ".71em")
          .style("text-anchor", "end")
          .text("ITM");

      svg.selectAll(".bar")
          .data(data)
          .enter().append("rect")
          .attr("class", "bar")
          .attr("x", function(d) { return x(d.letter); })
          .attr("width", x.rangeBand())
          .attr("y", function(d) { return y(d.frequency); })
          .attr("height", function(d) { return height - y(d.frequency); })
          .on('mouseover', tip.show)
          .on('mouseout', tip.hide)

    function type(d) {
      d.frequency = +d.frequency;
      return d;
    }
}
// Graf - KONEC

$(document).ready(function() {

	/**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
    $('#preberiPredlogoBolnika').change(function() {
        $("#kreirajSporocilo").html("");
        var podatki = $(this).val().split(",");
        $("#kreirajIme").val(podatki[0]);
        $("#kreirajPriimek").val(podatki[1]);
        $("#kreirajSpol").val(podatki[2]);
        $("#kreirajDatumRojstva").val(podatki[3]);
    });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
    $('#preberiObstojeciEHR').change(function() {
    	$("#preberiSporocilo").html("");
    	$("#preberiEHRid").val($(this).val());
    });

    /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split(",");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
	});

    /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

})
